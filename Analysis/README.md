# R markdown and rendered HTML workbook

These R markdown and HTML workbooks contain code and output of analysis presented in the manuscript.

## Content

`variants_structure.*`: Annotating TCGA variants in terms of protein (structural) consequences. [Click here](https://htmlpreview.github.io/?https://bitbucket.org/josef0731/zoomvarsomaticmutsig/raw/68e530dde539131eb21a14bd702591fb8156236d/Analysis/variants_structure.html) to view the rendered HTML notebook. 

`variants_selection.*`: Selection pressure in protein surface and core. [Click here](https://htmlpreview.github.io/?https://bitbucket.org/josef0731/zoomvarsomaticmutsig/raw/3ca18f9d60630ca00db480717be25fe3be0446c0/Analysis/variants_selection.html) to view the rendered HTML notebook.

`variants_signature.*`: Annotating mutational signatures in terms of protein (structural) consequences. [Click here](https://htmlpreview.github.io/?https://bitbucket.org/josef0731/zoomvarsomaticmutsig/raw/3b90c5df5953470016396cb2dac7bf37b785cbe1/Analysis/variants_signature.html) to view the rendered HTML notebook.

`SatMutSignatures.*`: Protein impact of SNVs occuring in different mutational contexts. [Click here](https://htmlpreview.github.io/?https://bitbucket.org/josef0731/zoomvarsomaticmutsig/raw/aa36bbdefef6119a5ffda24df9b7a47054a98e45/Analysis/SatMutSignatures.html) to view the rendered HTML notebook.

`boostDM_EVmutation.*`: Mapping mutations on scoring landscape defined by boostDM and EVmutation. [Click here](https://htmlpreview.github.io/?https://bitbucket.org/josef0731/zoomvarsomaticmutsig/raw/aa36bbdefef6119a5ffda24df9b7a47054a98e45/Analysis/boostDM_EVmutation.html) to view the rendered HTML notebook.

`predictor_analysis.*`: Analysing performance on predictors trained on experimental/computational DMS datasets. [Click here](https://htmlpreview.github.io/?https://bitbucket.org/josef0731/zoomvarsomaticmutsig/raw/aa36bbdefef6119a5ffda24df9b7a47054a98e45/Analysis/predictor_analysis.html) to view the rendered HTML notebook.

`variant_counts.txt`: A table of number of variants used in the predictors in comparison to ThermomutDB and ProThermDB. Data identical to Supplementary Table S1 and plot Figure 5B. Used in `predictor_analysis.Rmd`.